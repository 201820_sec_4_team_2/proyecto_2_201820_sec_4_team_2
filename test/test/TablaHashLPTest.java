package test;

import static org.junit.Assert.assertNull;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import model.data_structures.Queue;
import model.data_structures.TablaHashLP;

public class TablaHashLPTest {

	private TablaHashLP<String,Integer> prueba;

	public void setupScenario1()
	{
		prueba = new TablaHashLP<String,Integer>(5);
	}


	/**
	 * Revisa si se estan calculando bien los numero primos
	 */
	@Test
	public void testIsPrime() {
		setupScenario1();

		assertTrue("El numero 2 es primo", prueba.isPrime(2));
		assertTrue("El numero 3 es primo", prueba.isPrime(3));
		assertTrue("El numero 5 es primo", prueba.isPrime(5));
		assertTrue("El numero 7 es primo", prueba.isPrime(7));
		assertTrue("El numero 11 es primo", prueba.isPrime(11));
		assertTrue("El numero 13 es primo", prueba.isPrime(13));

		assertTrue("El numero 67 es primo", prueba.isPrime(67));
		assertTrue("El numero 83 es primo", prueba.isPrime(83));
		assertTrue("El numero 97 es primo", prueba.isPrime(97));

		assertTrue("El numero 599 es primo", prueba.isPrime(599));
		assertTrue("El numero 997 es primo", prueba.isPrime(997));

	}

	/**
	 * Revisa si funciona bien el metodo de calcular llaves
	 */
	@Test
	public void testYaExiste() {
		setupScenario1();
		//Llena la tabla con los numeros del 1 al 9 como llave respectivamente de las letras de la "a" a la "i"
		for (int i = 1; i <10; i++)
		{ 	
			prueba.put(i, Character.toString((char)('a'+i-1)));
		}
		assertTrue("Inicialmente deberian ser 9 parejas", prueba.size()==9);
		assertTrue("La llave 1 corresponde al valor a",prueba.get(1).equals("a"));

		prueba.put(1, "k");
		assertTrue("No debieron aumentar el numero de parejas", prueba.size()==9);
		assertTrue("Ahora la llave 1 debería corresponder al valor k",prueba.get(1).equals("k"));
	}

	/**
	 * Revisa que sucede cuando agrego elementos, y cuando se sobrepasa el factor de carga
	 * put, revTam; rehash
	 */
	@Test
	public void testPut() {
		setupScenario1();
		prueba.put(1, "a");
		prueba.put(2, "b");
		prueba.put(3, "c");
		prueba.put(4, "d");
		prueba.put(5, "e");
		assertTrue("La cantidad de parejas debería ser 5", prueba.size()==5);
		assertTrue("El tamaño del arreglo debe ser igual a 11", prueba.size()==11);

		prueba.put(6, "f");

		assertTrue("La cantidad de parejas debería ser 6", prueba.size()==6);
		assertTrue("El tamaño del arreglo debe ser igual a 23", prueba.size()==23);

		for (int i = 1; i <7; i++)
		{ 	
			prueba.put(6+i, Character.toString((char)('f'+i)));
		}
		assertTrue("La cantidad de parejas debería ser 12", prueba.size()==12);
		assertTrue("El tamaño del arreglo debe ser igual a 47", prueba.size()==47);
	}


	/**
	 * Revisa que sucede cuando busco elementos 
	 */
	@Test
	public void testGet() {
		setupScenario1();
		//Llena la tabla con los numeros del 1 al 10 como llave respectivamente de las letras de la a j
		for (int i = 1; i <11; i++)
		{ 	
			prueba.put(i, Character.toString((char)('a'+i-1)));
		}
		assertTrue("La llave 1 corresponde a la letra a, pero corresponde a "+ prueba.get(1), prueba.get(1).equals("a"));
		for (int i = 2; i <11; i++)
		{ 	
			assertTrue("La llave "+ i +" corresponde a la letra "+ Character.toString((char)('a'+i-1))+", pero corresponde a "+ prueba.get(i), prueba.get(i).equals(Character.toString((char)('a'+i-1))));
		}
	}


	/**
	 * Revisa que sucede cuando elimino elementos 
	 */
	@Test
	public void testDelete() {
		setupScenario1();
		//Llena la tabla con los numeros del 1 al 9 como llave respectivamente de las letras de la "a" a la "i"
		for (int i = 1; i <10; i++)
		{ 	
			prueba.put(i, Character.toString((char)('a'+i-1)));
		}
		assertTrue("Ahora el tamaño es igual a 9",prueba.size()==9);
		prueba.delete(1);
		assertTrue("Ahora el tamaño es igual a 8",prueba.size()==8);
		assertNull("No debería existir la llave", prueba.get(1));
	}

	/**
	 * Revisa que obtengo cuando pido que me den todas las llaves 
	 */
	@Test
	public void testKeys() {
		setupScenario1();
		//Llena la tabla con los numeros del 1 al 9 como llave respectivamente de las letras de la "a" a la "i"
		for (int i = 1; i <10; i++)
		{ 	
			prueba.put(i, Character.toString((char)('a'+i-1)));
		}
		Queue<Integer> llaves = (Queue<Integer>) prueba.keys();
		for (int i = 1; i <10; i++)
		{
			int j = llaves.dequeue();
			assertTrue("Deberia ser la llave "+i+" pero es " + j, j==i);
		}
	}
}

