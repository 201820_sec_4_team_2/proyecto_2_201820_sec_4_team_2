package test;

public class VOPrueba implements Comparable<VOPrueba>{

	private String estacionDeInicio;
	private String estacionFinal;
	private int duracion;
	
	public VOPrueba(String estacionDeInicio, String estacionFinal, int duracion) 
	{
		this.duracion = duracion;
		this.estacionDeInicio = estacionDeInicio;
		this.estacionFinal = estacionFinal;
	}
	
	@Override
	public int compareTo(VOPrueba other) {
		int comp = estacionDeInicio.compareTo(other.estacionDeInicio);
		if(comp!=0)
			return comp;
		else 
		{
			comp = estacionFinal.compareTo(other.estacionFinal);
			if(comp!=0)
				return comp;
			else 
			{
				if (duracion==other.duracion)
					return 0;
				else 
					return (duracion>other.duracion)? 1 : -1;
			}
		}
	}

	/**
	 * @return the estacionDeInicio
	 */
	public String getEstacionDeInicio() {
		return estacionDeInicio;
	}

	/**
	 * @param estacionDeInicio the estacionDeInicio to set
	 */
	public void setEstacionDeInicio(String estacionDeInicio) {
		this.estacionDeInicio = estacionDeInicio;
	}

	/**
	 * @return the estacionFinal
	 */
	public String getEstacionFinal() {
		return estacionFinal;
	}

	/**
	 * @param estacionFinal the estacionFinal to set
	 */
	public void setEstacionFinal(String estacionFinal) {
		this.estacionFinal = estacionFinal;
	}

	/**
	 * @return the duracion
	 */
	public int getDuracion() {
		return duracion;
	}

	/**
	 * @param duracion the duracion to set
	 */
	public void setDuracion(int duracion) {
		this.duracion = duracion;
	}
	
	


}
