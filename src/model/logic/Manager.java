package model.logic;

import API.IManager;

import controller.Controller.ResultadoCampanna;
import model.data_structures.*;
import model.vo.Bike;
import model.vo.BikeRoute;
import model.vo.RouteGSON;
import model.vo.RoutesGSON;
import model.vo.Station;
import model.vo.Trip;
import model.vo.VOSector;
import model.vo.BikeRoute;
import model.vo.Station;
import model.vo.Trip;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.*;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.Iterator;

import com.google.gson.Gson;
import com.opencsv.CSVReader;

public class Manager implements IManager {
	//Ruta del archivo de datos 2017-Q1
	public static final String TRIPS_Q1 = "./data/Divvy_Trips_2017_Q1.csv";

	//Ruta del archivo de trips 2017-Q2
	public static final String TRIPS_Q2 = "./data/Divvy_Trips_2017_Q2.csv";

	//Ruta del archivo de trips 2017-Q3
	public static final String TRIPS_Q3 = "./data/Divvy_Trips_2017_Q3.csv";

	//Ruta del archivo de trips 2017-Q4
	public static final String TRIPS_Q4 = "./data/Divvy_Trips_2017_Q4.csv";

	//Ruta del archivo de stations 2017-Q1-Q2
	public static final String STATIONS_Q1_Q2 = "./data/Divvy_Stations_2017_Q1Q2.csv";

	//Ruta del archivo de stations 2017-Q3-Q4
	public static final String STATIONS_Q3_Q4 = "./data/Divvy_Stations_2017_Q3Q4.csv";

	//Ruta del archivo de stations 2017-Q3-Q4
	public static final String BIKE_ROUTES = "./data/CDOT_Bike_Routes_2014_1216-transformed(1).json";

	//ATRIBUTOS
	private List<BikeRoute> rutas ;
	private int tamanioListaRutas;

	private double minLon;
	private double minLat;
	private double maxLon;
	private double maxLat;

	private int divLa;
	private int divLo;


	private List<Trip> listaTrips;
	private int tamanioListaTrip;


	private List<Station> listaEstaciones;
	private int tamanioListaStation;

	private Station[] arrEstaciones;


	private ArbolRojoNegro<Long, Trip> viajesFechaTerminacion;
	private TablaHashLP<Station, Integer> estacionesId;

	private TablaHashLP<VOSector, Integer> sectores;

	@Override
	public void cargarDatos(String rutaTrips, String rutaStations, String rutaRutas) {

		// Cargar viajes

		sectores = new TablaHashLP<>(10);
		viajesFechaTerminacion = new ArbolRojoNegro<>();
		estacionesId = new TablaHashLP<>(100);
		listaEstaciones = new List<Station>();
		listaTrips = new List<Trip>();
		minLon = Long.MAX_VALUE;
		minLat = Long.MAX_VALUE;
		maxLon = Long.MIN_VALUE;
		maxLat = Long.MIN_VALUE;

		try {
			String[] archivosTrips = rutaTrips.split(":");
			for (int i = 0; i < archivosTrips.length; i++) {
				@SuppressWarnings("resource")
				CSVReader reader = new CSVReader(new FileReader(archivosTrips[i]));

				String[] linea;
				linea = reader.readNext();
				linea = reader.readNext();

				while ( linea != null){
					Trip trip = new Trip(Integer.parseInt(linea[0]),linea[1],linea[2],Integer.parseInt(linea[3]), Integer.parseInt(linea[4]), Integer.parseInt(linea[5]), Integer.parseInt(linea[7]), linea[10]);
					ZonedDateTime zdt = trip.getStopTime().atZone(ZoneId.of("America/Chicago"));
					viajesFechaTerminacion.put(zdt.toInstant().toEpochMilli(), trip);
					listaTrips.add(trip);
					linea = reader.readNext();
				}
				tamanioListaTrip += listaTrips.size();
			}
		} 
		catch (IOException e) {
			e.printStackTrace();
		}

		//Cargar estaciones

		try {
			String[] archivosStations = rutaStations.split(":");
			for (int i = 0; i < archivosStations.length; i++) {
				@SuppressWarnings("resource")
				CSVReader reader = new CSVReader(new FileReader(archivosStations[i]));

				String[] linea;
				linea = reader.readNext();
				linea = reader.readNext();
				while ( linea != null){
					Station stat = new Station(Integer.parseInt(linea[0]), linea[1], linea[2], Double.parseDouble(linea[3]), Double.parseDouble(linea[4]), Integer.parseInt(linea[5]), linea[6]);
					if (stat.getLatitude()>maxLat)
						maxLat = stat.getLatitude();
					else if(stat.getLatitude()<minLat)
						minLat = stat.getLatitude();
					if (stat.getLongitude()>maxLon)
						maxLon = stat.getLatitude();
					else if(stat.getLongitude()<minLon)
						minLon = stat.getLatitude();

					estacionesId.put(stat.getId(), stat);
					listaEstaciones.add(stat);
					linea = reader.readNext();
				}
				tamanioListaStation += listaEstaciones.size();
				pasarEstacionesArreglo();
			}
		} 
		catch (IOException e) {
			e.printStackTrace();
		}

		// CARGAR RUTAS

		if(tamanioListaRutas==0 )
		{
			rutas = new List<>();
			Gson gson = new Gson();
			BufferedReader br = null;	
			minLon = Long.MAX_VALUE;
			minLat = Long.MAX_VALUE;
			maxLon = Long.MIN_VALUE;
			maxLat = Long.MIN_VALUE;

			try {
				br = new BufferedReader(new FileReader(rutaRutas));
				RoutesGSON routes = gson.fromJson(br, RoutesGSON.class);
				if (routes != null) {
					for (RouteGSON t : routes.getRoutes()) {
						BikeRoute ruta = new BikeRoute(t.getId(), t.getBIKEROUTE(), t.getTheGeom(), t.getSTREET(), t.getFSTREET(), t.getTSTREET(),11);
						rutas.add(ruta);
						double[] lat = ruta.getLat();
						for(int i = 0; i<lat.length; i++)
						{
							if(lat[i]<minLat)
								minLat = lat[i];
							if(lat[i]>maxLat)
								maxLat = lat[i];

						}
						double[] lon = ruta.getLon();
						for(int i = 0; i<lon.length; i++)
						{
							if(lon[i]<minLon)
								minLon = lon[i];
							if(lon[i]>maxLon)
								maxLon = lon[i];
						}
					}
				}
				tamanioListaRutas = rutas.size();
				for (BikeRoute ruta : rutas)
				{
					System.out.println(ruta.getId());
				}
			} 
			catch (FileNotFoundException e) 
			{
				e.printStackTrace();
			} 
			finally 
			{
				if (br != null) {
					try {
						br.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}
	}

	public void pasarEstacionesArreglo()
	{
		arrEstaciones = new Station[tamanioListaStation];
		int i = 0;
		for (Station estacion : listaEstaciones)
		{
			arrEstaciones[i] = estacion;
			i++;
		}
	}

	@Override
	public ICola<Trip> A1(int n, LocalDate fechaTerminacion) {	
		long inicio = fechaTerminacion.atStartOfDay().atZone(ZoneId.of("America/Chicago")).toInstant().toEpochMilli();
		long finale = inicio + 86400000; //milisegundos un dia
		Queue<Trip> a1 = new Queue<>();
		Iterator<Long> iterViajes = viajesFechaTerminacion.keysInRange(inicio, finale);
		while (iterViajes.hasNext())
		{
			Long llave = (Long) iterViajes.next();
			Trip viaje = viajesFechaTerminacion.get(llave);
			Station estacion = estacionesId.get(viaje.getEndStationId());
			if(estacion.getBykeCapacity()>=n)
				a1.enqueue(viaje);
		}
		return a1;
	}

	@Override
	public ILista<Trip> A2(int n) {
		TablaHashLP<List<Trip>, Integer> tabla = crearTablaHashDuracion();
		n = (n%2!=0)? n+1 : n;
		return tabla.get(n/2);
	}

	public TablaHashLP<List<Trip>, Integer>  crearTablaHashDuracion()
	{
		TablaHashLP<List<Trip>, Integer> tabla = new TablaHashLP<>(100);
		int i = 0;
		Trip[] muestraAOrdenar = new Trip[tamanioListaTrip]; 
		for (Trip viaje : listaTrips )	
		{
			muestraAOrdenar[i] = viaje;
			i++;
		}
		sortTrip(muestraAOrdenar, Trip.DURATION, i-1);

		int maxDuracion = muestraAOrdenar[i-1].getTripDuration()/60;
		maxDuracion = (maxDuracion%2==0) ? maxDuracion + 2 : maxDuracion + 1;
		for (int j = 2; j<maxDuracion; j+=2)
		{
			tabla.put(j/2, new List<Trip>());
		}

		int limMay = 2;
		for (int h = 0 ; h < i ; h++)
		{
			Trip viajeAct = muestraAOrdenar[i];
			if (viajeAct.getTripDuration()/60<=limMay)
				tabla.get(limMay/2).add(viajeAct);
			else
			{
				limMay+=2;
				tabla.get(limMay/2).add(viajeAct);
			}
		}
		return tabla;
	}

	@Override
	public ILista<Trip> A3(int n, LocalDate fecha){
		List<Trip> a3 = new List<>();
		long inicio = fecha.atStartOfDay().atZone(ZoneId.of("America/Chicago")).toInstant().toEpochMilli();
		long finale = inicio + 86400000; //milisegundos un dia
		MaxHeapCP<ViajeDistancia> viajesPorDistancia = new MaxHeapCP<>(tamanioListaTrip);	
		Iterator<Long> iterViajes = viajesFechaTerminacion.keysInRange(inicio, finale);
		int numViajesClasifican = 0;
		while (iterViajes.hasNext())
		{
			Long llave = (Long) iterViajes.next();
			Trip viaje = viajesFechaTerminacion.get(llave);
			try {
				viajesPorDistancia.agregar(new ViajeDistancia(viaje, estacionesId.get(viaje.getStartStationId()), estacionesId.get(viaje.getEndStationId())));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			numViajesClasifican++;
		}

		for (int i = 0; i<numViajesClasifican && i<n; i++)
		{
			a3.add(viajesPorDistancia.max().getViaje());
		}
		return a3;
	}

	@Override
	public ILista<Bike> B1(int limiteInferior, int limiteSuperior) {
		// TODO Auto-generated method stub
		Trip[] muestraAOrdenar = new Trip[tamanioListaTrip]; 
		int i = 0;

		for (Trip viaje : listaTrips)
		{
			muestraAOrdenar[i] = viaje;
			i++;
		}

		sortTrip(muestraAOrdenar, Trip.BIKE_ID,i-1);
		Bike[] muestraBicicletas = new Bike[tamanioListaTrip]; 
		int k = 0;
		double distanciaAcumulada = 0.0;
		double tiempoAcumulado = 0.0;
		int numeroViajes = 1;
		for (i = 0; i<tamanioListaTrip-1; i++)
		{
			tiempoAcumulado += muestraAOrdenar[i].getTripDuration();
			Station stat1 = findStation(muestraAOrdenar[i].getStartStationId());
			Station stat2 = findStation(muestraAOrdenar[i].getEndStationId());
			distanciaAcumulada += stat1.calcularDistancia(stat2);

			if(muestraAOrdenar[i].getBikeId()==muestraAOrdenar[i+1].getBikeId())	
				numeroViajes++;
			if(muestraAOrdenar[i].getBikeId()!=muestraAOrdenar[i+1].getBikeId())
			{	
				muestraBicicletas[k] = new Bike(muestraAOrdenar[i].getBikeId(), numeroViajes, (int)distanciaAcumulada, (int)tiempoAcumulado);
				distanciaAcumulada = 0.0;
				tiempoAcumulado = 0.0;
				numeroViajes=1;
				k++;
			}
			//Caso para saber que hacer con la última bicicleta
			if(i==tamanioListaTrip-2)
			{
				Station statU1 = findStation(muestraAOrdenar[i+1].getStartStationId());
				Station statU2 = findStation(muestraAOrdenar[i+1].getEndStationId());
				//Osea que la úlitma bicicleta tiene un bikeid diferente
				if(numeroViajes==1)
				{
					distanciaAcumulada = statU1.calcularDistancia(statU2); 
					muestraBicicletas[k] = new Bike(muestraAOrdenar[i+1].getBikeId(), numeroViajes, (int)distanciaAcumulada, (int)muestraAOrdenar[i+1].getTripDuration());
				}
				else 
				{
					tiempoAcumulado += muestraAOrdenar[i+1].getTripDuration();
					distanciaAcumulada += statU1.calcularDistancia(statU2);
					muestraBicicletas[k] = new Bike(muestraAOrdenar[i+1].getBikeId(), numeroViajes, (int)distanciaAcumulada, (int)tiempoAcumulado);
				}
			}
		}
		ArbolRojoNegro<Integer,Bike> arbolDeBicicletas = new ArbolRojoNegro<Integer,Bike>();
		int n=0;
		while (muestraBicicletas[n] != null){
			arbolDeBicicletas.put(muestraBicicletas[n].getTotalDuration(), muestraBicicletas[n]);
			n++;
		}
		Iterator<Integer> keysEnRango = arbolDeBicicletas.keys(limiteInferior, limiteSuperior);
		List<Bike> bicicletasEnRango = new List<Bike>();
		while(keysEnRango.hasNext()){
			bicicletasEnRango.add(arbolDeBicicletas.get(keysEnRango.next()));
		}
		return bicicletasEnRango;
	}

	@Override
	public ILista<Trip> B2(String estacionDeInicio, String estacionDeLlegada, int limiteInferiorTiempo, int limiteSuperiorTiempo) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int[] B3(String estacionDeInicio, String estacionDeLlegada) {
		// TODO Auto-generated method stub
		return new int[] {0, 0};
	}

	@Override
	public ResultadoCampanna C1(double valorPorPunto, int numEstacionesConPublicidad, int mesCampanna) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public double[] C2(int LA, int LO) {
		divLa = LA;
		divLo = LO;
		double deltaLon = maxLon - minLon;
		double deltaLat = maxLat - minLat;
		double latInSector = 0.0;
		double latFinSector = 0.0;
		double lonInSector = 0.0;
		double lonFinSector = 0.0;
		int i = 0;
		for (i = 0; i<LO; i++)
		{
			lonInSector = minLon + i*(deltaLon/LO);
			lonFinSector = minLon + (i+1)*(deltaLon/LO);
			for (int j = 0; j<LA; j++)
			{
				latInSector= maxLat - j*(deltaLat/LA);
				latFinSector= maxLat - (j+1)*(deltaLat/LA);

				VOSector sector = new VOSector(10*(j)+i, lonFinSector, lonInSector, latFinSector, latInSector);
				sectores.put(10*(j)+i, sector);
			}

		}
		agregarLasRutasACadaSector();
		agregarLasEstacionesACadaSector();
		double arr[] = new double[4];
		arr[0] = maxLat;
		arr[1] = maxLon;
		arr[2] = minLat;
		arr[3] = minLon;
		return arr;
	}


	public void	agregarLasEstacionesACadaSector()
	{
		VOSector sectAct = null;
		double latInSector = 0.0;
		double latFinSector = 0.0;
		double lonInSector = 0.0;
		double lonFinSector = 0.0; 

		double latStat= 0.0;
		double lonStat = 0.0;

		int numFila = 0;
		int numColumna = 0;


		for(Station stat: listaEstaciones)
		{
			latStat = stat.getLatitude();
			lonStat = stat.getLongitude();
			for (int i = 0; i<divLa; i++)
			{
				sectAct = sectores.get(i*10);
				latInSector = sectAct.getMinLat();
				latFinSector = sectAct.getMaxLat();
				if((latInSector>latStat)&&(lonStat>latFinSector))
				{
					numFila=i;
					break;
				}
			}
			for (int i = 0; i<divLo; i++)
			{
				sectAct = sectores.get(numFila*10 + i);
				lonInSector = sectAct.getMinLong();
				lonFinSector = sectAct.getMaxLong();
				if((lonInSector<lonStat)&&(lonStat<lonFinSector))
				{
					numColumna=i;
					break;
				}
			}		
			sectores.get(numFila*10+numColumna).agregarEstacion(stat);
		}

	};


	public void	agregarLasRutasACadaSector()
	{
		VOSector sectAct = null;
		double latInSector = 0.0;
		double latFinSector = 0.0;
		double lonInSector = 0.0;
		double lonFinSector = 0.0; 

		double latInRuta = 0.0;
		double latFinRuta = 0.0;
		double lonInRuta = 0.0;
		double lonFinRuta = 0.0; 

		int numFila = 0;
		int numColumna = 0;


		for(BikeRoute ruta : rutas)
		{
			latInRuta = ruta.getLat()[0];
			latFinRuta = ruta.getLat()[ruta.getLat().length-1];
			lonInRuta = ruta.getLon()[0];
			lonFinRuta = ruta.getLon()[ruta.getLon().length-1];
			for (int i = 0; i<divLa; i++)
			{
				sectAct = sectores.get(i*10);
				latInSector = sectAct.getMinLat();
				latFinSector = sectAct.getMaxLat();
				if(((latInSector>latInRuta)&&(latInRuta>latFinSector))||((latInSector>latFinRuta)&&(latFinRuta>latFinSector)))
				{
					numFila=i;
					break;
				}
			}
			for (int i = 0; i<divLo; i++)
			{
				sectAct = sectores.get(numFila*10 + i);
				lonInSector = sectAct.getMinLong();
				lonFinSector = sectAct.getMaxLong();
				if(((lonInSector<lonInRuta)&&(lonInRuta<lonFinSector))||((lonInSector<lonFinRuta)&&(lonFinRuta<lonFinSector)))
				{
					numColumna=i;
					break;
				}
			}		
			sectores.get(numFila*10+numColumna).agregarRuta(ruta);
		}

	}

	/**
	 * @return the minLon
	 */
	public double getMinLon() {
		return minLon;
	}


	/**
	 * @return the minLat
	 */
	public double getMinLat() {
		return minLat;
	}


	/**
	 * @return the maxLon
	 */
	public double getMaxLon() {
		return maxLon;
	}


	/**
	 * @return the maxLat
	 */
	public double getMaxLat() {
		return maxLat;
	}

	@Override
	public int darSector(double latitud, double longitud) {

		int numFila = 0;
		int numColumna = 0;
		VOSector sectAct = null;
		double latInSector = 0.0;
		double latFinSector = 0.0;
		double lonInSector = 0.0;
		double lonFinSector = 0.0; 

		VOSector sectorEncontrado = null;
		for (int i = 0; i<divLa; i++)
		{
			sectAct = sectores.get(i*10);
			latInSector = sectAct.getMinLat();
			latFinSector = sectAct.getMaxLat();

			if((latInSector>=latitud)&&(latitud>=latFinSector))
			{
				numFila=i;
				break;
			}
		}
		for (int i = 0; i<divLo; i++)
		{
			sectAct = sectores.get(numFila*10 + i);
			lonInSector = sectAct.getMinLong();
			lonFinSector = sectAct.getMaxLong();

			if((lonInSector<=longitud)&&(longitud<=lonFinSector))
			{
				sectorEncontrado = sectAct;
				numColumna=i;
				break;
			}
		}
		return 10*numColumna+numFila;
	}

	@Override
	public ILista<Station> C3(double latitud, double longitud) {
		return sectores.get(darSector(latitud, longitud)).getEstacionesQueEstanAca();
	}

	@Override
	public ILista<BikeRoute> C4(double latitud, double longitud) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ILista<BikeRoute> C5(double latitudI, double longitudI, double latitudF, double longitudF){
		// TODO Auto-generated method stub
		return null;
	}

	//ORDENAMIENTO VOTrips

	public void sortTrip(Trip[] arreglo, int caso, int tope){
		Trip[] aux = new Trip[arreglo.length];
		sortTrip(arreglo, aux, 0, tope, caso);
	}

	public void sortTrip(Trip[] arreglo, Trip[] aux, int low, int high, int caso){
		if(high <= low) return;
		int middle = low + (high - low)/2;
		sortTrip(arreglo,aux,low, middle,caso);
		sortTrip(arreglo,aux,middle+1,high,caso);
		mergeTrip(arreglo,aux,low,middle,high, caso);
	}

	private void mergeTrip(Trip[] arreglo,Trip[] aux, int low, int middle, int high, int caso) {
		// TODO Auto-generated method stub
		for (int k = low; k <= high; k++)
			aux[k] = arreglo[k];
		int i = low;
		int j = middle+1;
		for (int k = low; k<=high;k++){
			if (i>middle) arreglo[k] = aux[j++];
			else if (j>high) arreglo[k] = aux[i++];
			else if (less(aux[j], aux[i], caso)) arreglo[k] = aux[j++];
			else arreglo[k] = aux[i++];
		}
	}

	private  boolean less( Trip v, Trip w, int caso) {
		return v.compareTo(w, caso) < 0;
	}

	public Station findStation(int statId)
	{
		Station respuesta=null;
		for (Station actual: listaEstaciones){
			if (actual.getId() == statId)
				respuesta = actual;
		}
		return respuesta;
	}
}
