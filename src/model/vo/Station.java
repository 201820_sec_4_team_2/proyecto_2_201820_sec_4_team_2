package model.vo;

import java.time.LocalDateTime;
import java.time.Month;

public class Station implements Comparable<Station> {


	public final static int EARTH_RADIOUS = 6731000;


	private int id;
	private String stationName;
	private String city;
	private double latitude;
	private double longitude;
	private int bykeCapacity;
	private LocalDateTime startDate;

	public Station(int id, String stationName, String city, double latitude, double longitude, int bykeCapacity, String startDate){
		this.id = id;
		this.stationName = stationName;
		this.city = city;
		this.latitude = latitude;
		this.longitude = longitude;
		this.bykeCapacity = bykeCapacity;
		/**
		 * DateTimeFormatter formatter1  = null;
		if(startDate.length()==16)
			formatter1 = DateTimeFormatter.ofPattern("MM/dd/yyyy HH:mm");
		else if (startDate.length()==13)
			formatter1 = DateTimeFormatter.ofPattern("M/d/yyyy H:mm");	
		else if (startDate.substring(1, 2).equals("/")&&startDate.length()==15)
			formatter1 = DateTimeFormatter.ofPattern("M/dd/yyyy HH:mm");
		else if (startDate.substring(1, 2).equals("/")&&startDate.length()==14)
			formatter1 = DateTimeFormatter.ofPattern("M/dd/yyyy H:mm");
		else 
			formatter1 = DateTimeFormatter.ofPattern("MM/d/yyyy HH:mm");
		 **/
		String fecha = startDate.substring(0, startDate.indexOf(" "));
		String[] fechaAr = fecha.split("/");
		String hora = startDate.substring(startDate.indexOf(" ")).trim();
		String[] horaAr = hora.split(":");
		this.startDate = LocalDateTime.of(Integer.valueOf(fechaAr[2]), Month.of(Integer.valueOf(fechaAr[0])),Integer.valueOf(fechaAr[1]),Integer.valueOf(horaAr[0]),Integer.valueOf(horaAr[1]));
	}


	public int getId() {
		return id;
	}	

	public String getStationName(){
		return stationName;
	}

	public LocalDateTime getStartDate() {
		return startDate;
	}

	@Override
	public int compareTo(Station o) {
		if(id==o.id)
			return 0;
		else if(id<o.id)
			return -1;
		else 
			return 1;
	}

	public double calcularDistancia(Station otherStation)
	{
		double lat1 = Math.toRadians(latitude);
		double lat2 = Math.toRadians(otherStation.latitude);
		double difLat = Math.toRadians(otherStation.latitude-latitude);
		double difLon = Math.toRadians(otherStation.longitude-longitude);
		double a = Math.sin(difLat/2)*Math.sin(difLat/2) + Math.cos(lat1)*Math.cos(lat2)*Math.sin(difLon/2)*Math.sin(difLon/2);
		double c = 2*Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
		double distance = EARTH_RADIOUS * c;

		return distance;
	}
	
	
	public String getCity() {
		return city;
	}


	public void setCity(String city) {
		this.city = city;
	}


	public double getLatitude() {
		return latitude;
	}


	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}


	public double getLongitude() {
		return longitude;
	}


	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}


	public int getBykeCapacity() {
		return bykeCapacity;
	}


	public void setBykeCapacity(int bykeCapacity) {
		this.bykeCapacity = bykeCapacity;
	}
}
