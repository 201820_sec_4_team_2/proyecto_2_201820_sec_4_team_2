package model.vo; 

import model.data_structures.*;

import java.util.ArrayList;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RoutesGSON {

	@SerializedName("routes")
	@Expose
	//Para leer el Gson, intente usar List, pero no me dejaba, así que use un arraylist
	private ArrayList<RouteGSON> routes = new ArrayList<RouteGSON>();

	public ArrayList<RouteGSON> getRoutes() {
		return routes;
	}

	public void setRoutes(ArrayList<RouteGSON> routes) {
		this.routes = routes;
	}

}