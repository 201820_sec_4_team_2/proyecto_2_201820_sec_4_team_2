package model.vo;

public class BikeRoute implements Comparable<BikeRoute>{

	public final static String BIKE_LANE = "BIKE LANE";
	public final static String SHARED_LANE = "SHARED-LANE";
	public final static String BUFFERED_BIKE_LANE = "BUFFERED BIKE LANE";
	public final static String CYCLE_TRACK = "CYCLE TRACK" ;
	public final static String NEIGHBORHOOD_GREENWAY = "NEIGHBORHOOD GREENWAY";
	public final static String ACCESS_PATH = "ACCESS PATH";

	private String id;
	private String type;
	private double[] lat;
	private double[] lon;
	private String refStreet;
	private String lim1Street;
	private String lim2Street;
	private double totDistance;
	
	public BikeRoute(String id, String type, String theGeom, String refStreet, String lim1Street, String lim2Street, double totDistance) {
		this.id = id;
		this.type = type;
		String s1 = theGeom.substring(12);
		String s2 = s1.substring(0, s1.length()-1);
		String[] latLong = s2.split(" ");
		lat = new double[latLong.length/2];
		lon = new double[latLong.length/2];
		for(int i = 0; i<latLong.length; i++)
		{
			if(i%2==0)
			{
				lon[i/2]= Double.parseDouble(latLong[i]);
			}
			else
			{
				lat[i/2]=Double.parseDouble(latLong[i].substring(0, latLong[i].length()-1));
			}
		}
		this.refStreet = refStreet;
		this.lim1Street = lim1Street;
		this.lim2Street = lim2Street;
		this.totDistance = totDistance;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}


	/**
	 * @return the lat
	 */
	public double[] getLat() {
		return lat;
	}

	/**
	 * @param lat the lat to set
	 */
	public void setLat(double[] lat) {
		this.lat = lat;
	}

	/**
	 * @return the lon
	 */
	public double[] getLon() {
		return lon;
	}

	/**
	 * @param lon the lon to set
	 */
	public void setLon(double[] lon) {
		this.lon = lon;
	}

	/**
	 * @return the refStreet
	 */
	public String getRefStreet() {
		return refStreet;
	}

	/**
	 * @param refStreet the refStreet to set
	 */
	public void setRefStreet(String refStreet) {
		this.refStreet = refStreet;
	}

	/**
	 * @return the lim1Street
	 */
	public String getLim1Street() {
		return lim1Street;
	}

	/**
	 * @param lim1Street the lim1Street to set
	 */
	public void setLim1Street(String lim1Street) {
		this.lim1Street = lim1Street;
	}

	/**
	 * @return the lim2Street
	 */
	public String getLim2Street() {
		return lim2Street;
	}

	/**
	 * @param lim2Street the lim2Street to set
	 */
	public void setLim2Street(String lim2Street) {
		this.lim2Street = lim2Street;
	}

	/**
	 * @return the totDistance
	 */
	public double getTotDistance() {
		return totDistance;
	}

	/**
	 * @param totDistance the totDistance to set
	 */
	public void setTotDistance(double totDistance) {
		this.totDistance = totDistance;
	}


	@Override
	public int compareTo(BikeRoute o) {
		// TODO Auto-generated method stub
		return 0;
	}
}
