package model.data_structures;

import java.util.Iterator;

public class List<T extends Comparable<T>> implements ILista<T>{

	private Node<T> head;
	private Node<T> tail;
	private int size;

	public List()
	{
		head=null;
		tail=null;
		size = 0;
	}

	public List(T[] arreglo)
	{
		for (int i = 0; i<arreglo.length;i++)
		{
			if(arreglo[i]!=null)
			{
				add(arreglo[i]);
			}
		}
	}


	public Node<T> getHead()
	{
		return head;
	}

	public void add(T elem) {
		Node<T> element = new Node<T>(elem, null, null);
		if(isEmpty())
		{
			head = element;
			tail = element;
		}
		else
		{
			tail.cambiarSiguiente(element);
			tail = element;
		}
		size++;
	}

	@Override
	public T remove(T elem) {
		Node<T> act = head;
		Node<T> actPrevious = null;
		while(act!=null)
		{
			if(elem==head)
			{
				head = null;
				return head.getElement();
			}
			if(elem==(act.getElement()))
			{
				actPrevious.cambiarSiguiente(act.darSiguiente());
				return act.getElement();
			}
		}
		return null;
	}

	@Override
	public int size() {
		return size;
	}

	/**public T get(T elem) {
		Node<T> act = head;
		while(act!=null)
		{
			if(elem==(act.getElement()))
				return act.getElement();
		}
		return null;
	}
	 */

	@Override
	public T get(int pos) {
		Node<T> act = head;
		for(int i = 0; i<pos;i++)
			act = act.darSiguiente();
		return act.getElement();
	}

	@Override
	public boolean isEmpty() {
		return size==0;
	}

	@Override
	public Iterator<T> iterator() {
		return new ListIterator<T>(head);	
	}

	@Override
	public T get(T elem) {
		// TODO Auto-generated method stub
		return null;
	}

}