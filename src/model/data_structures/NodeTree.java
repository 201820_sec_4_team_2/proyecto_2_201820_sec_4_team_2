package model.data_structures;

public class NodeTree<Key extends Comparable<Key>, Value> {
	private Key key;           
    private Value val;         
    private NodeTree left, right;  
    //Rojo es true, negro es false
    private boolean color;    
    private int size;          

    public NodeTree (Key key, Value val, boolean color, int size) {
        this.key = key;
        this.val = val;
        this.color = color;
        this.size = size;
    }
    
    public void cambiarAColor(boolean colorNuevo){
    	color = colorNuevo;
    }
    
    public Key darLlave(){
    	return key;
    }
    
    public NodeTree darHijoIzq(){
    	return left;
    }
    public NodeTree darHijoDer(){
    	return right;
    }
    public void asignarHijoIzq(NodeTree hijoNuevo){
    	left = hijoNuevo;
    }
    public void asignarHijoDer(NodeTree hijoNuevo){
    	right = hijoNuevo;
    }
    public void asignarValor(Value value){
    	val = value;
    }
    
    public boolean esRojo(){
    	return color;
    }
    
    public void cambiarTamaño(int newSize){
    	size = newSize;
    }

	public int darTamaño() {
		// TODO Auto-generated method stub
		return size;
	}
	
	public void asignarLlave(Key keyNueva){
		key = keyNueva;
	}
	
	public Value darValor(){
		return val;
	}
}