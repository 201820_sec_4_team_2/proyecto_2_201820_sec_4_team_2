package model.data_structures;

import java.util.Iterator;
import java.util.stream.IntStream;

public class TablaHashLP <V, K extends Comparable<K>> implements ITablaHash<V, K> {
	private static final double MAXIMO_FACTOR_CARGA = 0.75;

	private int n;          
	private int m;         
	private K[] keys;   
	private V[] vals;

	private double factorDeCarga;
	
	public TablaHashLP(int capacity) {
		factorDeCarga= MAXIMO_FACTOR_CARGA;
		m = capacity;
		n = 0;
		keys = (K[])  new Comparable[m];
		vals = (V[]) new Object[m];
	}
	public TablaHashLP(int capacity, double pFactorDeCarga) {
		this.factorDeCarga = pFactorDeCarga;
		m = capacity;
		n = 0;
		keys = (K[])  new Comparable[m];
		vals = (V[]) new Object[m];
	}
	public boolean isEmpty() {
		return size() == 0;
	}

	private int hash(K key) {
		return (key.hashCode() & 0x7fffffff) % m;
	}


	@Override
	public void put(K llave, V valor) {
		// TODO Auto-generated method stub
		if (llave == null) throw new IllegalArgumentException("Argumento es null");

		if (valor == null) {
			delete(llave);
			return;
		}

		if (n >= m*factorDeCarga) resize(2*m);

		int i;
		for (i = hash(llave); keys[i] != null; i = (i + 1) % m) {
			if (keys[i].equals(llave)) {
				vals[i] = valor;
				return;
			}
		}
		keys[i] = llave;
		vals[i] = valor;
		n++;
	}

	private void resize(int i) {
		int siguientePrimo = i;
		
		
		TablaHashLP<V, K> temp = new TablaHashLP<V, K>(siguientePrimo);
		for (int j = 0; j < m; j++) {
			if (keys[j] != null) {
				temp.put(keys[j], vals[j]);
			}
		}
		keys = temp.keys;
		vals = temp.vals;
		m    = temp.m;
	}
	public boolean isPrime(int number) {
		if(n==2) return true;
		if (n%2==0) return false;
		for(int i=3;i*i<=n;i+=2) {
			if(n%i==0)
				return false;
		}
		return true;	}
	@Override
	public V get(K llave) {
		// TODO Auto-generated method stub
		if (llave == null) throw new IllegalArgumentException("Argumento es null");
		for (int i = hash(llave); keys[i] != null; i = (i + 1) % m)
			if (keys[i].equals(llave))
				return vals[i];
		return null;	
	}

	public boolean contains(K key) {
        return get(key) != null;
    }
	
	@Override
	public V delete(K llave) {
		// TODO Auto-generated method stub
		 if (llave == null) throw new IllegalArgumentException("Argumento es null");
	        if (!contains(llave)) return null;

	        int i = hash(llave);
	        while (!llave.equals(keys[i])) {
	            i = (i + 1) % m;
	        }

	        V aBorrar = get(llave);
	        keys[i] = null;
	        vals[i] = null;

	        i = (i + 1) % m;
	        while (keys[i] != null) {
	            // delete keys[i] an vals[i] and reinsert
	            K   keyToRehash = keys[i];
	            V valToRehash = vals[i];
	            keys[i] = null;
	            vals[i] = null;
	            n--;
	            put(keyToRehash, valToRehash);
	            i = (i + 1) % m;
	        }

	        n--;

	        return aBorrar;
	}

	@Override
	public Iterable<K> keys() {
		Queue<K> queue = new Queue<K>();
		for (int i = 0; i < m; i++)
			if (keys[i] != null) queue.enqueue(keys[i]);
		return queue;
	}
	public int size() {
		return n;
	}
	@Override
	public Iterator<K> iterator() {
		// TODO Auto-generated method stub
		return null;
	}
}