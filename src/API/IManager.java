package API;

import model.data_structures.ICola;
import model.data_structures.ILista;
import model.vo.Bike;
import model.vo.BikeRoute;
import model.vo.Station;
import model.vo.Trip;
import model.vo.VOSector;

import java.time.LocalDate;
import java.time.LocalDateTime;

import controller.Controller.ResultadoCampanna;

public interface IManager {
    
	/**
	 * Actualizar la informacion del sistema con los datos seleccionados por el usuario y generar/actualizar las estructuras de datos necesarias.
	 * Caso Especial: si rutaTrips y rutaStations son la cadena vacia (""), los datos del sistema deben reiniciarse con un conjunto de trips y de estaciones vacios.
	 * @param rutaTrips ruta del archivo de trips que se va a utilizar
	 * @param rutaStations ruta del archivo de stations que se va a utilizar
	 * @param dataBikeRoutes ruta del archivo de cicloruta que se va a utilizar
	 */
	void cargarDatos(String rutaTrips, String rutaStations, String dataBikeRoutes);
	
	/**
     * Generar una cola con los viajes que terminaron en una estaci�n que tiene capacidad n en la fecha de terminaci�n indicada
     * @param n indica la capacidad de la estaci� en la que termin� el viaje
     * @param fechaTerminacion indica la fecha en que se termin� el viaje
     * @return Cola con los viajes que cumplen las condiciones mencionada en el enunciado
     */
    ICola<Trip> A1(int n, LocalDate fechaTerminacion);

    /**
     * Buscar los viajes que tienen duraciones similares.
     * @param n duraci�n en minutos de lo que deben durar los viajes del grupo que se quiere identificar
     * @return Lista con los viajes
     */
    ILista<Trip> A2(int n);

    
    /**
     * Busca los n viajes con los recorridos m�s largos	en una fecha dada
     * @param n n�mero de viajes que se desea consultar
     * @param fecha Fecha en la que se desean consultar los viajes
     * @return Lista con los viajes ordenados 
     */
    ILista<Trip> A3(int n, LocalDate fecha);
    
    /**
     * Buscar	bicicletas	para	mantenimiento
     * @param limiteInferior indica el tiempo minimo total recorrido que debe tener una bicicleta
     * @param limiteSuperior indica el tiempo m�ximo total recorrido que debe tener una bicicleta
     * @return Lista con las bicicletas
     */
    ILista<Bike> B1(int limiteInferior, int limiteSuperior);


    /**
     * B�squeda	de	viajes	por	estaciones	de	salida	y	llegada	en	un	rango	de	tiempo.
     * @param fechaInicial Fecha inicial del periodo de consulta
     * @param fechaFinal Fecha final del periodo de consulta
     * @param limiteInferiorTiempo tiempo m�nimo (en segundos) de lo que debe durar el recorrido para ser considerado
     * @param limiteSuperiorTiempo tiempo m�ximo (en segundos) de lo que debe durar el recorrido para ser considerado
     * @return Lista con los viajes ordenados
     */
    ILista<Trip> B2(String estacionDeInicio, String estacionDeLlegada, int limiteInferiorTiempo, int limiteSuperiorTiempo);

    /**
     * Mostrar los viajes (ordenados cronologicamente)realizados por una bicicleta con el identificador dado que tengan
     * una duracion menor al valor de tiempo maximo dado y que hayan sido realizados por una persona del genero dado
     * @param bikeId El identificador de la bicicleta
     * @param tiempoMaximo La duracion maxima de los viajes
     * @param genero El genero de la persona que realizo los viajes
     * @return Lista con los viajes ordenados
     */
	int[] B3(String estacionDeInicio, String estacionDeLlegada);

	ResultadoCampanna C1(double valorPorPunto, int numEstacionesConPublicidad, int mesCampanna);

	double[] C2(int lA, int lO);

	int darSector(double latitud, double longitud);

	ILista<Station> C3(double latitud, double longitud);

	ILista<BikeRoute> C4(double latitud, double longitud);

	ILista<BikeRoute> C5(double latitudI, double longitudI, double latitudF, double longitudF);
}
